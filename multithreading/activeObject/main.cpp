
#include <functional>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <exception>
#include <utility>
#include <deque>



template<typename T>
    class ConcurrentQueue{
  public:
    explicit ConcurrentQueue()
    {}
    void push(const T& t)
    {
      std::lock_guard<std::mutex> lock(mutex_);
      queue_.emplace_back(t);
    }

    T pop()
    {
      std::lock_guard<std::mutex> lock(mutex_);
      T v = queue_.front();
      queue_.pop_front();
      return v;
    }

    bool empty() const
    {
      std::lock_guard<std::mutex> lock(mutex_);
      return queue_.empty();
    }

    size_t size() const
    {
      std::lock_guard<std::mutex> lock(mutex_);
      return queue_.size();
    }

    ConcurrentQueue(const ConcurrentQueue&) = delete;
    ConcurrentQueue& operator=(const ConcurrentQueue&) = delete;

  private:
    mutable std::mutex mutex_;
    std::deque<T> queue_;
  };





    class ActiveObject{
    public:
        using Message = std::function<void()>;
        using ErrorCB = std::function<void(std::exception_ptr)>;
        using QueueData = std::pair<Message, ErrorCB>;
            using MutexType = std::mutex;

        explicit ActiveObject()
                : done_(false),
                  thread_(std::thread{[this]{ Run();}})
            {}

        ~ActiveObject()
        {
                Send([this](){ done_ = true;});
                 thread_.join();
        }
        void Send(const Message& m)
        {
            queue_.push(std::make_pair(m, ActiveObject::ErrorCB()));
            cv_.notify_one();
        }

        void Send(const Message& m, const ErrorCB& on_error)
        {
            queue_.push(std::make_pair(m, on_error));
            cv_.notify_one();
        }

        ActiveObject(const ActiveObject&) = delete;
        ActiveObject& operator=(const ActiveObject&) = delete;


    private:
        void with_error_handler(const ActiveObject::QueueData& m)
        {
            try
            {
                m.first();
            }
            catch(...)
            {
                m.second(std::current_exception());
            }
        }

        void Run()
        {
            while(!done_)
            {
                std::unique_lock<MutexType> lock(mutex_);
            // queue size check due to possible spurious wakeups ( not from being notified)
                cv_.wait(lock, [this](){return !queue_.empty();});
            ActiveObject::QueueData m = queue_.pop();
            if(m.second)
            {
                    with_error_handler(m);
            }
            else
            {
                    // any exceptions better be handled in m
                    // maybe allow the installation of a generic error handler in the AO
                    // which is then always used in this case
                    m.first();
            }
            }
        }

    private:
        bool done_;
    // used for condition variable
        MutexType mutex_;
        std::condition_variable cv_;
        ConcurrentQueue<QueueData> queue_;
        std::thread thread_;
    };



    class Test{
    public:
      void Write(std::string , std::function<void(bool)> cb)
      {
        a.Send(
             [cb](){
               // Do job here ( such as write data, whatever)
               // signal to caller
               cb(true);
             });
      }
    private:
      ActiveObject a;
    };

    int main()
    {
      Test t;
      t.Write("dandy",
          [](bool success)
          {
            printf("Success: %i\n", success);
          });
      return EXIT_SUCCESS;
    }
