#include <string>
#include <functional>
#include <future>
#include <iostream>
#include <queue>
#include <thread>
#include <vector>

class ThreadPool
{
  public:
    ThreadPool(const ThreadPool &) = delete;
    ThreadPool(ThreadPool &&) = delete;
    ThreadPool &operator=(const ThreadPool &) = delete;
    ThreadPool &operator=(ThreadPool &&) = delete;

    ThreadPool(size_t nr_threads):stop(false)
    {
        for (size_t i = 0; i < nr_threads; ++i)
        {
            std::thread worker([this]() {
                while (true)
                {
                    std::function<void()> task;
                    /* pop a task from queue, and execute it. */
                    {
                        std::unique_lock lock(mtx);
                        cv.wait(lock, [this]() { return stop || !tasks.empty(); });
                        if (stop && tasks.empty())
                            return;
                        /* even if stop = 1, once tasks is not empty, then
                         * excucte the task until tasks queue become empty
                         */
                        task = std::move(tasks.front());
                        tasks.pop();
                    }
                    task();
                }
            });
            workers.emplace_back(std::move(worker));
        }
    }


    virtual ~ThreadPool()
    {
        /* stop thread pool, and notify all threads to finish the remained tasks. */
         {
             std::unique_lock<std::mutex> lock(mtx);
             stop = true;
         }
         cv.notify_all();
         for (auto &worker : workers)
             worker.join();
    }

    template <class F, class... Args>
    std::future<std::result_of_t<F(Args...)>> enqueue(F &&f, Args &&...args)
    {/* The return type of task `F` */
       using return_type = std::result_of_t<F(Args...)>;

       /* wrapper for no arguments */
       auto task = std::make_shared<std::packaged_task<return_type()>>(
           std::bind(std::forward<F>(f), std::forward<Args>(args)...));

       std::future<return_type> res = task->get_future();
       {
           std::unique_lock lock(mtx);

           if (stop)
               throw std::runtime_error("The thread pool has been stop.");

           /* wrapper for no returned value */
           tasks.emplace([task]() -> void { (*task)(); });
       }
       cv.notify_one();
       return res;
    }

  private:
    std::vector<std::thread> workers;
    std::queue<std::function<void()>> tasks;

    /* For sync usage, protect the `tasks` queue and `stop` flag. */
    std::mutex mtx;
    std::condition_variable cv;
    bool stop;
};

// Creer une vecteur de thread qui prend en entr�s des fonctions qui se trouvent dans le vecteur de taches
// Tant que le vecteur de taches n'est n'est pas vide  on continue, sinon on attend
int main()
{
    /* Compute square of numbers. */
    ThreadPool pool(4);
    std::vector<std::future<int>> results;

    for (int i = 0; i < 8; ++i)
    {
        auto future = pool.enqueue([i] {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            return i * i;
        });
        results.emplace_back(std::move(future));
    }

    for (auto &result : results)
        std::cout << result.get() << ' ';
    std::cout << std::endl;
    return 0;
}
