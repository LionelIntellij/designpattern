#include <string>
#include <iostream>

class BaseComponent;

class Mediator {
 public:
  virtual void Notify(std::string event) const = 0;
};


class BaseComponent {
 protected:
  Mediator *mediator_;

 public:
  BaseComponent(Mediator *mediator = nullptr) : mediator_(mediator) {
  }

  void DoA() {
    std::cout << "Component 1 does A.\n";
    mediator_->Notify( "A");
  }

  void set_mediator(Mediator *mediator) {
    mediator_ = mediator;
  }
};

class ConcreteMediator : public Mediator {
 private:
  BaseComponent *component1_;

 public:
  ConcreteMediator(BaseComponent *c1) : component1_(c1) {
    this->component1_->set_mediator(this);
  }
  void Notify(std::string event) const override {
    if (event == "A") {
      std::cout << "Mediator reacts on A and triggers following operations:\n";
      component1_->DoA();
    }
  }
};

//classe mediator qui fait les opérations pointeur du composant passé en argument
//Le pointeur mediator est passé au component pour permettre de notifier le mediator

int  main()
{

    BaseComponent c1 ;
    ConcreteMediator mediator(&c1);
     std::cout << "Client triggers operation A.\n";
     c1.DoA();
     std::cout << "\n";
     std::cout << "Client triggers operation D.\n";

    return 0;
}
