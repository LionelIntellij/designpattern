#include <string>
#include <memory>
#include <vector>
#include <iostream>

class Handler {
 public:
  virtual std::shared_ptr<Handler>& SetNext(std::shared_ptr<Handler> &handler) = 0;
  virtual std::string Handle(std::string request) = 0;
};
/**
 * The default chaining behavior can be implemented inside a base handler class.
 */
class AbstractHandler : public Handler {
  /**
   * @var Handler
   */
 private:
  std::shared_ptr<Handler> next_handler_;

 public:
  AbstractHandler() : next_handler_(nullptr) {
  }
  std::shared_ptr<Handler>& SetNext(std::shared_ptr<Handler> &handler) override {
    this->next_handler_ = handler;
     return handler;
  }

  std::string Handle(std::string request) override {
    if (this->next_handler_) {
      return this->next_handler_->Handle(request);
    }
    return {};
  }
};

class MonkeyHandler : public AbstractHandler {
 public:
  std::string Handle(std::string request) override {
    if (request == "Banana") {
      return "Monkey: I'll eat the " + request + ".\n";
    } else {
      return AbstractHandler::Handle(request);
    }
  }
};
class SquirrelHandler : public AbstractHandler {
 public:
  std::string Handle(std::string request) override {
    if (request == "Nut") {
      return "Squirrel: I'll eat the " + request + ".\n";
    } else {
      return AbstractHandler::Handle(request);
    }
  }
};


class DogHandler : public AbstractHandler {
 public:
  std::string Handle(std::string request) override {
    if (request == "MeatBall") {
      return "Dog: I'll eat the " + request + ".\n";
    } else {
      return AbstractHandler::Handle(request);
    }
  }
};

// associer les demandeurs et les récepteurs par les chaines de responsabilité
//chanque handler teste si la request leur est dédié

int  main()
{
    std::shared_ptr<Handler> monkey (new MonkeyHandler);
    std::shared_ptr<Handler> squirrel(new SquirrelHandler);
    std::shared_ptr<Handler> dog(new DogHandler);
    monkey->SetNext(squirrel)->SetNext(dog);

    std::vector<std::string> food = {"Nut", "Banana", "Cup of coffee"};
    for (const std::string &f : food) {
      std::cout << "Client: Who wants a " << f << "?\n";
      const std::string result = monkey->Handle(f);
      if (!result.empty()) {
        std::cout << "  " << result;
      } else {
        std::cout << "  " << f << " was left untouched.\n";
      }
    }

    return 0;
}
