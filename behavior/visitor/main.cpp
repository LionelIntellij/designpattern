#include <string>
#include <iostream>

class Component;



class Visitor {
 public:
   void VisitComponent(const Component *element);

};


class Component {
  /**
   * Note that we're calling `visitConcreteComponentA`, which matches the
   * current class name. This way we let the visitor know the class of the
   * component it works with.
   */
 public:
  void Accept( Visitor &visitor) const {
    visitor.VisitComponent(this);
  }
  /**
   * Concrete Components may have special methods that don't exist in their base
   * class or interface. The Visitor is still able to use these methods since
   * it's aware of the component's concrete class.
   */
  std::string ExclusiveMethodOfComponent() const {
    return "ConcreteComponent";
  }
 };


void Visitor::VisitComponent(const Component *element)
{
 std::cout << element->ExclusiveMethodOfComponent() << " + ConcreteVisitor\n";
}


//Sépare les algorythmes des object sur lesquel visitor opère
// on passe la classe visitor en argument qui prend lui meme un argument la class component via la fonctionne de
//visite qui appelle par la suite  les fonction du component.

int  main()
{
    Component component;
    Visitor visitor;
    component.Accept(visitor);
    return 0;
}
