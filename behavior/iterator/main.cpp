#include <iostream>
#include <string>
#include <vector>
#include <memory>


template <typename T, typename U>
class Iterator {
 public:
  typedef typename std::vector<T>::iterator iter_type;
  Iterator(U *p_data) : m_p_data_(p_data), m_currPos(0){
  }

  Iterator<T,U>& begin() {
   m_currPos = 0;
   return *this;
  }

  const T& Next() {
      if(m_currPos< m_p_data_->m_currlen_)
          return m_p_data_->m_data_.get()[m_currPos++];
      else
          return end();
  }

  const T& end() {
     return m_end;
  }

  const T& Current() {
      if(m_currPos< m_p_data_->m_currlen_)
          return m_p_data_->m_data_.get()[m_currPos];
      else
          return end();
  }

 private:
  U* m_p_data_;
  size_t m_currPos;
  T m_end;
};

/**
 * Generic Collections/Containers provides one or several methods for retrieving
 * fresh iterator instances, compatible with the collection class.
 */



template <class T>
class Container {
  friend class Iterator<T, Container>;

 public:

  struct free_deleter
  {
      void operator()(void * p)
      {
          delete static_cast<T*>(p);
      }
  };
  void push_back(T a) {
      if (m_currlen_ >= m_maxlen_) {
          m_maxlen_ *= 2;
          void* newData = realloc(m_data_.get(), m_maxlen_);

          if (m_data_) {
              m_data_.release();
          }
          m_data_.reset(static_cast<T*>(newData));
      }
      m_data_.get()[m_currlen_++] = a;
  }

  Iterator<T, Container>& iterator()
  {
    return  m_it;
  }

  Container(): m_maxlen_(1),
      m_currlen_(0),
      m_data_(new T(1)),
      m_it(this)
  {

  }

  ~Container()=default;

 private:
  size_t m_maxlen_;
  size_t m_currlen_;
  std::unique_ptr<T, free_deleter> m_data_;
  Iterator<T, Container> m_it;
};



int main()
{
    Container<int> test;
    test.push_back(1);
    test.push_back(4);
    test.push_back(5);
    test.push_back(40);
    Iterator<int,Container<int>> itContainer = test.iterator();
    for(Iterator<int,Container<int>> it = itContainer.begin(); it.Current() != itContainer.end(); it.Next() )
    {
        std::cout<<it.Current()<<std::endl;
    }
    return 0;
}
