#include <iostream>
class State;
class Context;

class State {
  /**
   * @var Context
   */
 protected:
  Context *context_;

 public:
  virtual ~State() {
  }

  void set_context(Context *context) {
    context_ = context;
  }
  virtual void Handle() = 0;

};



class Context {
  /**
   * @var State A reference to the current state of the Context.
   */
 private:
  State *state_;

 public:
  Context(State *state) : state_(nullptr) {
    TransitionTo(state);
  }
  ~Context() {
    delete state_;
  }
  /**
   * The Context allows changing the State object at runtime.
   */
  void TransitionTo(State *state) {
    std::cout << "Context: Transition to " << typeid(*state).name() << ".\n";
    if (state_ != nullptr)
      delete state_;
    state_ = state;
    state_->set_context(this);
  }
  /**
   * The Context delegates part of its behavior to the current State object.
   */
  void Request() {
    state_->Handle();
  }

};



class ConcreteStateA : public State {
 public:


  void Handle() override {
    std::cout << "ConcreteStateA handles request2.\n";
  }
};


class ConcreteStateB : public State {
 public:

  void Handle() override {
    std::cout << "ConcreteStateB handles request2.\n";
    std::cout << "ConcreteStateB wants to change the state of the context.\n";
    context_->TransitionTo(new ConcreteStateA);
  }
};





int  main()
{
    ConcreteStateB stateB;
    Context context(&stateB);
    ConcreteStateA stateA;
    context.TransitionTo(&stateA);
    return 0;
}
