#include <iostream>


class Subject {
public:
virtual	void Request() const  {
        std::cout << "RealSubject: Handling request.\n";
    }
    virtual ~Subject() = default;
};

/**
 * The Proxy has an interface identical to the RealSubject.
 */
class Proxy : public Subject {
    /**
     * @var RealSubject
     */
private:
    Subject* real_subject_;

public:
    Proxy(Subject* real_subject) : real_subject_(new Subject(*real_subject)) {
    }

    ~Proxy() {
        delete real_subject_;
        //delete it durring the deleting of the object contrary to the decorator
    }
    /**
     * The most common applications of the Proxy pattern are lazy loading,
     * caching, controlling the access, logging, etc. A Proxy can perform one of
     * these things and then, depending on the result, pass the execution to the
     * same method in a linked RealSubject object.
     */
    void Request() const override {
            this->real_subject_->Request();
    }
};

//on hérite de la cible et l'interface devient le proxy.
// Le proxy est la seule interface entre l'objet et l'utilisateur. Il détruit l'objet

int main()
{

Subject subject;
Proxy proxySubject(&subject);
proxySubject.Request();


}
