#include <algorithm>
#include <iostream>
#include <list>
#include <string>
#include <memory>
/**
 * The base Component class declares common operations for both simple and
 * complex objects of a composition.
 */
class Component
{
  /**
   * @var Component
   */
 protected:
  std::shared_ptr<Component> parent_;
  std::string m_name;

 public:
  virtual ~Component() {}
  void setName(const std::string & name) { m_name = name;}
  void SetParent(const std::shared_ptr<Component> &parent) {
    this->parent_ = parent;
  }
  Component *GetParent() const {
      if(parent_)
        return parent_.get();
      return nullptr;
  }

  virtual void Add(std::shared_ptr<Component>& ) {}
  virtual void Remove(std::shared_ptr<Component> &) {}

  virtual bool IsComposite() const {
    return false;
  }

  virtual std::string Operation() const = 0;
};


class Leaf : public Component {
 public:
  std::string Operation() const override {
    return "Leaf";
  }
};



class Composite : public Component, public std::enable_shared_from_this<Composite>{
 protected:
  std::list<std::weak_ptr<Component>> children_;

 public:

  void Add(std::shared_ptr<Component> &component) override {
    children_.push_back(component);
    component->SetParent(shared_from_this());
  }

  void Remove(std::shared_ptr<Component> &component) override
  {
      children_.remove_if([component] (const auto& p ){
          const auto & pShared = p.lock();
            if(pShared  && component)
                return pShared == component;
            return false;});
    component->SetParent(nullptr);
  }

  bool IsComposite() const override {
    return true;
  }

  std::string Operation() const override {
    std::string result;
    const auto & childBack = children_.back().lock();

    for (const auto & c : children_) {
      const auto & child = c.lock();
      if (child == childBack) {
        result += child->Operation();
      } else {
        result += child->Operation() + "+";
      }
    }
    return "Branch(" + result + ")";
  }
};


 int main()
{

     std::shared_ptr<Component> tree = std::shared_ptr<Composite>(new Composite);
     std::shared_ptr<Component> branch1 = std::shared_ptr<Composite>(new Composite);

     std::shared_ptr<Component> leaf_1 = std::shared_ptr<Leaf>(new Leaf);
     std::shared_ptr<Component> leaf_2 = std::shared_ptr<Leaf>(new Leaf);
     branch1->Add(leaf_1);
     branch1->Add(leaf_2);

     std::shared_ptr<Component> leaf_3 = std::shared_ptr<Leaf>(new Leaf);
      std::shared_ptr<Component> leaf_4 = std::shared_ptr<Leaf>(new Leaf);
     std::shared_ptr<Component> branch2 = std::shared_ptr<Composite>(new Composite);
     branch2->Add(leaf_3);
     branch2->Add(leaf_4);
     tree->Add(branch1);
     tree->Add(branch2);
     std::cout<<" Tree Operation " <<tree->Operation()<<std::endl;
     tree->Remove(branch1);
     std::cout<<" Tree operation "<<tree->Operation()<<std::endl;
    return 0;
}
