#include <string>
#include <iostream>

class Component {
public:
    virtual std::string Operation() const {
        return "ConcreteComponent";
    }

    virtual ~Component() =default;
};

/**
 * The base Decorator class follows the same interface as the other components.
 * The primary purpose of this class is to define the wrapping interface for all
 * concrete decorators. The default implementation of the wrapping code might
 * include a field for storing a wrapped component and the means to initialize
 * it.
 */
class Decorator : public Component {
    /**
     * @var Component
     */
protected:
    Component* component_;

public:
    Decorator(Component* component) : component_(component) {
    }
    /**
     * The Decorator delegates all work to the wrapped component.
     */
    std::string Operation() const override {
        return component_->Operation();
    }
};



int main()
{
    /**
     * This way the client code can support both simple components...
     */
    Component* simple = new Component;
    std::cout << "Client: I've got a simple component:\n";

    std::cout << "\n\n";
    /**
     * ...as well as decorated ones.
     *
     * Note how decorators can wrap not only simple components but the other
     * decorators as well.
     */
    Component* decorator = new Decorator(simple);
    delete decorator;
    delete simple;
    return 0;
    //the component is always controlled by the client delete it in the main contray to the proxy
}
