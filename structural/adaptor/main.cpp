#include <string>
#include <algorithm>

//L’Adaptateur modifie l’interface d’un objet existant,
//alors que le Décorateur améliore un objet sans toucher à son interface.


class Target {
 public:
  virtual ~Target() = default;

  virtual std::string Request() const {
    return "Target: The default target's behavior.";
  }
};


class Adaptee {
 public:
  std::string SpecificRequest() const {
    return ".eetpadA eht fo roivaheb laicepS";
  }
};


class Adapter : public Target {
 private:
  Adaptee *adaptee_;

 public:
  Adapter(Adaptee *adaptee) : adaptee_(adaptee) {}
  std::string Request() const override {
    std::string to_reverse = this->adaptee_->SpecificRequest();
    std::reverse(to_reverse.begin(), to_reverse.end());
    return "Adapter: (TRANSLATED) " + to_reverse;
  }
};

//s'adapte a une interface ou  aune fonction. l'adaptateur hérite de la cible tout au moins .
//Elle peut hériter des classes des deux classes aussi

int main()
{
    Adaptee *adaptee = new Adaptee;
     Adapter *adapter = new Adapter(adaptee);
     adapter->Request();
     delete adaptee;
     delete adapter;
    return 0;
}
