#include <string>

class Implementation {
 public:
  ~Implementation() = default;
  std::string OperationImplementation()
    {
        return "ConcreteImplementationA: Here's the result on the platform A.\n";
    }
};



class Abstraction {
  /**
   * @var Implementation
   */
 protected:
  Implementation* implementation_;

 public:
  Abstraction(Implementation* implementation) : implementation_(implementation) {
  }

  ~Abstraction() =default;

  std::string Operation() const {
    return "Abstraction: Base operation with:\n" +
           this->implementation_->OperationImplementation();
  }
};

//pont passe en argument dans le constructeur l'impléménetation, l'impléménetation est gardé en attribut et est appelé
//dans chaque fonction.

int main()
{
    Implementation impl;
    Abstraction abstraction(&impl);
    return 0;
}
