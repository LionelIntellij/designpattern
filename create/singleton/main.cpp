#include <iostream>
#include <memory>
#include <mutex>


class Singleton {

public:
	static Singleton* getInstance(double value)
	{	//thread safe two thread can access instance simultaneous
		std::lock_guard<std::mutex> lock(m_mutex);
		if (!m_instance)
			m_instance = new Singleton(value);
		return m_instance;
	}

	void doSomething()
	{
		int toto = m_value;
	}
	//not be clonable
	Singleton(Singleton& other) = delete;

	//not assignable
	void operator=(const Singleton&) = delete;

private:
	Singleton(double value);
	double m_value;
	static Singleton* m_instance;
	static std::mutex m_mutex;
};

Singleton::Singleton(double value):
	m_value(value)
{

}

Singleton* Singleton::m_instance{nullptr};
std::mutex Singleton::m_mutex;


int main()
{

	Singleton* singleton = Singleton::getInstance(5.);
	singleton->doSomething();
	return 0;

}

