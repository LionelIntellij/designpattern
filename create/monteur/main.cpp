#include <iostream>
#include <vector>


class Product{
    public:
    std::vector<std::string> parts_;
    void ListParts()const{
        std::cout << "Product parts: ";
        for (size_t i=0;i<parts_.size();i++){
            if(parts_[i]== parts_.back()){
                std::cout << parts_[i];
            }else{
                std::cout << parts_[i] << ", ";
            }
        }
        std::cout << "\n\n"; 
    }
};

/**
 * The Concrete Builder classes follow the Builder interface and provide
 * specific implementations of the building steps. Your program may have several
 * variations of Builders, implemented differently.
 */
class Builder  {
    private:

    Product* product;

    /**
     * A fresh builder instance should contain a blank product object, which is
     * used in further assembly.
     */
    public:

        Builder(){
        this->Reset();
    }

    ~Builder(){
        delete product;
    }

    void Reset(){
        this->product= new Product();
    }
    /**
     * All production steps work with the same product instance.
     */

    void ProducePartA()const {
        this->product->parts_.push_back("PartA1");
    }

    void ProducePartB()const {
        this->product->parts_.push_back("PartB1");
    }

    void ProducePartC()const {
        this->product->parts_.push_back("PartC1");
    }

    Product* GetProduct() { return product; }
};

/*
Le monteur nécessite de créer beaucoup nouvelles classes et fonction, ce qui accroit la complexité générale du code.
*/



class Director{
    /**
     * @var Builder
     */
    private:
    Product* m_product;
    
    public:
    Director()
    {
    }

    void build_product()
    {
        Builder builder;
        builder.ProducePartA();
        builder.ProducePartC();
        m_product = builder.GetProduct();
    }

};

int main()
{
  Director director;
    director.build_product();

}
