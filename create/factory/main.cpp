#include <string>
#include <iostream>

class Product {
 public:
  virtual ~Product() {}
  virtual std::string Operation() const = 0;
};

/**
 * Concrete Products provide various implementations of the Product interface.
 */
class ConcreteProduct1 : public Product {
 public:
  std::string Operation() const override {
    return "{Result of the ConcreteProduct1}";
  }
};
class ConcreteProduct2 : public Product {
 public:
  std::string Operation() const override {
    return "{Result of the ConcreteProduct2}";
  }
};

enum  class ProductType
{
	Prod1,
	Prod2
};

Product* creator( ProductType type)
{

switch(type)
{
case  ProductType::Prod1:
		return new ConcreteProduct1;
case ProductType::Prod2:
		return new ConcreteProduct2;
	default:
		break;
}
return nullptr;

}

/*
 Le code peut devenir plus complexe que nécessaire, car ce patron de conception impose l’ajout de nouvelles classes et interfaces.

*/

int main()
{

Product * prod1 = creator(ProductType::Prod1);
std::cout<<prod1->Operation()<<std::endl;
delete  prod1;


Product * prod2 = creator(ProductType::Prod2);
std::cout<<prod2->Operation()<<std::endl;
delete prod2;
return 0;
}
